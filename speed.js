	var fontSize = 3;
	var color = 3;
	var position = 1;
	var keypress = 0;
	var bgColor = "black";
	var bgColorIndex = 0;
	var bgOpacity = 0;
	var charOpacity =255;
	var playRate =1;
	var message = "Helloworld";

	//listen to button

	document.addEventListener("keydown", function(inEvent){
        if (inEvent.keyCode === 34) {
		    playRate = playRate - 0.25;
			getWebosmediaIdS(setPlayRate);
			console.log ("[P- button hit]");
			}
		else if (inEvent.keyCode === 33) {
		    playRate = playRate + 0.25;
			getWebosmediaIdS(setPlayRate);
			console.log ("[P+ button hit]");
			}			
	});


	
	//Get mediaIdS 

	function getWebosmediaIdS(func) {
	                var mediaIdS = document.querySelector('video').mediaId;
					//setTimeout(console.log("[success] Id is " + mediaIdS),1000);
					setTimeout(func(mediaIdS),300);
	}

    //notification
    
    	function notify(message){
    	  webOS.service.request("luna://com.webos.notification", {
    	    method:"createToast",
    	    parameters: {
    	        "sourceId":"com.lampa.tv",
    			"message": message
    			},
    	      onSuccess: function (result) {
    	        
    		console.log("[Notification fired] " );
    		},
    	      onFailure: function (result) {
    	        console.log( "[Notification failed] ");
    	        }
    	 });
    	}




	//Playback rate 

	function setPlayRate(mediaIdS){
	 if (playRate < 0.25 ) {playRate = 0.25}
	   else if (playRate > 2 ) {playRate = 2}
	   else {
	    webOS.service.request("luna://com.webos.media", {
	    method:"setPlayRate",
	    parameters: {
			"playRate": playRate,
			"mediaId": mediaIdS,
			"audioOutput":true
			},
	      onSuccess: function (result) {
	        message = "Playback speed: " + playRate;	
	    	console.log("[playRate speed][Success] "  + playRate + " "+ JSON.stringify(result));
	    	notify(message);
		},
	      onFailure: function (result) {
	        console.log( "[playRate speed][fail][" + result.errorCode + "] " + result.errorText );
	        }
	 });
	   }
	}
